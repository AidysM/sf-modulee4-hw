import wget


def path_to_favicon(url):
    domain_name = url.split('/')[2]
    path_to_fav = 'http://' + domain_name + '/static/assets/favicon.ico'

    return path_to_fav


url = input('Введите URL: ')
fav_url = path_to_favicon(url)
favicon = wget.download(fav_url)
print(favicon)
