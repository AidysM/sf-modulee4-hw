FROM python:3.9 as builder

ARG REVISION

RUN mkdir -p /favicondownload/

WORKDIR /favicondownload

RUN pip install wget

COPY . .

#FROM alpine:3.14.1
#
#ARG BUILD_DATE
#ARG VERSION
#ARG REVISION
#
##LABEL maintainer="stefanprodan"
#
#RUN addgroup -S app \
#    && adduser -S -G app app \
#    && apk --no-cache add \
#    ca-certificates curl netcat-openbsd
#
#WORKDIR /home/app
#
#COPY --from=builder /favicondownload/bin/favicondownload .
#COPY --from=builder /favicondownload/bin/podcli /usr/local/bin/podcli
#COPY ./ui ./ui
#RUN chown -R app:app ./
#
#USER app
#
#CMD ["./favicondownload"]

CMD ["python", "./favicondownload.py"]
